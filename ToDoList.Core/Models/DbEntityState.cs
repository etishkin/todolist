﻿namespace ToDoList.Core.Models
{
    public enum DbEntityState
    {
        Active,
        Inactive, 
        Deleted
    }
}