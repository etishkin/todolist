﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoList.Core.Models
{
    public class Note
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [MaxLength(50, ErrorMessage = "Max length is 50 symbols")]
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }

        [Required(ErrorMessage = "User is required")]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [Required(ErrorMessage = "Priority is required")]
        public int PriorityId { get; set; }
        public virtual Priority Priority { get; set; }

        public DbEntityState State { get; set; }

        public Note()
        {
            DateCreated = DateTime.Now;
        }

        public object ToJson()
        {
            return new
            {
                id = Id,
                name = Name,
                time = DateCreated,
                description = Description ?? "",
                priorityId = PriorityId,
                priority = Priority != null ? Priority.Name : "",
                state = (int)State
            };
        }
    }
}