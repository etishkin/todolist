﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Reflection;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ToDoList.Core.Models
{
    public interface IApplicationDbContext : IDisposable
    {
        IDbSet<Note> Notes { get; set; }
        IDbSet<Priority> Priorities { get; set; }
        IDbSet<ApplicationUser> Users { get; set; }

        Database Database { get; }
        DbEntityEntry Entry(object entity);
        int SaveChanges();
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {
        }

        public IDbSet<Note> Notes { get; set; }
        public IDbSet<Priority> Priorities { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.Conventions.AddFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}