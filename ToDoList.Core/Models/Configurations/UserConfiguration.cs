﻿using System.Data.Entity.ModelConfiguration;
using ToDoList.Core.Models;

namespace ToDoList.Core
{
    internal class UserConfiguration: EntityTypeConfiguration<ApplicationUser>
    {
        public UserConfiguration()
        {
            HasMany(u => u.Notes).WithRequired(n => n.User).HasForeignKey(n => n.UserId).WillCascadeOnDelete(false); 
            HasMany(u => u.Priorities).WithRequired(p => p.User).HasForeignKey(p => p.UserId).WillCascadeOnDelete(false); 
        }
    }
}