using System.Data.Entity.ModelConfiguration;
using ToDoList.Core.Models;

namespace ToDoList.Core
{
    internal class PriorityConfiguration : EntityTypeConfiguration<Priority>
    {
        public PriorityConfiguration()
        {
            HasMany(p => p.Notes).WithRequired(n => n.Priority).HasForeignKey(n => n.PriorityId).WillCascadeOnDelete(false); ;
        }
    }
}