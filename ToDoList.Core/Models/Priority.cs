﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ToDoList.Core.Models
{
    public class Priority
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [MaxLength(128, ErrorMessage = "Max length is 128 symbols")]
        public string Name { get; set; }

        [Required(ErrorMessage = "User is required")]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<Note> Notes { get; set; }
        public DbEntityState State { get; set; }

        public object ToJson()
        {
            return new
            {
                id = Id,
                name = Name,
                state = (int) State
            };
        }
    }
}