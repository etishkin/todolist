﻿using System;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ToDoList.Core
{
    internal class DateTimeConvention : Convention
    {
        public DateTimeConvention()
        {
            Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
        }
    }
}