namespace ToDoList.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedState : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notes", "State", c => c.Int(nullable: false));
            AddColumn("dbo.Priorities", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Priorities", "State");
            DropColumn("dbo.Notes", "State");
        }
    }
}
