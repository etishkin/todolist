namespace ToDoList.Core.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class ModelsAttributes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Notes", "PriorityId", "dbo.Priorities");
            DropForeignKey("dbo.Notes", "UserId", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Priorities", "UserId", "dbo.ApplicationUsers");
            AlterColumn("dbo.Notes", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Priorities", "Name", c => c.String(nullable: false, maxLength: 128));
            AddForeignKey("dbo.Notes", "PriorityId", "dbo.Priorities", "Id");
            AddForeignKey("dbo.Notes", "UserId", "dbo.ApplicationUsers", "Id");
            AddForeignKey("dbo.Priorities", "UserId", "dbo.ApplicationUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Priorities", "UserId", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Notes", "UserId", "dbo.ApplicationUsers");
            DropForeignKey("dbo.Notes", "PriorityId", "dbo.Priorities");
            AlterColumn("dbo.Priorities", "Name", c => c.String());
            AlterColumn("dbo.Notes", "Name", c => c.String());
            AddForeignKey("dbo.Priorities", "UserId", "dbo.ApplicationUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Notes", "UserId", "dbo.ApplicationUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Notes", "PriorityId", "dbo.Priorities", "Id", cascadeDelete: true);
        }
    }
}
