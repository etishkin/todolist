using System.Data.Entity.Migrations;
using ToDoList.Core.Models;

namespace ToDoList.Core.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
        }
    }
}
