﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using ToDoList.Core.Models;

namespace ToDoList.Core.Services
{
    public interface INotesService
    {
        Note Find(int id);
        IEnumerable<Note> List(string userId);
        Note AddOrUpdate(Note priority);

        Note Complete(int id, string userId);
        Note Delete(int id, string userId);
    }

    public class NotesService : INotesService
    {
        protected readonly IApplicationDbContext context;

        public NotesService(IApplicationDbContext context)
        {
            this.context = context;
        }


        public Note Find(int id)
        {
            return context.Notes.Find(id);
        }

        public IEnumerable<Note> List(string userId)
        {
            return
                context.Notes.Include(n => n.Priority)
                    .Where(n => n.UserId == userId && n.State != DbEntityState.Deleted);
        }

        public Note AddOrUpdate(Note note)
        {
            try
            {
                context.Notes.AddOrUpdate(note);
                context.SaveChanges();
                return note;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Note Complete(int id, string userId)
        {
            return ChangeState(id, userId, DbEntityState.Inactive);
        }

        public Note Delete(int id, string userId)
        {
            return ChangeState(id, userId, DbEntityState.Deleted);
        }

        protected Note ChangeState(int id, string userId, DbEntityState state)
        {
            var entity = Find(id);
            if (entity == null || entity.UserId != userId)
            {
                return null;
            }
            entity.State = state;
            return AddOrUpdate(entity);
        }
    }

}