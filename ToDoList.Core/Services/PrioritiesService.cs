﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Microsoft.AspNet.Identity;
using ToDoList.Core.Models;

namespace ToDoList.Core.Services
{
    public interface IPrioritiesService
    {
        Priority Find(int id);
        IEnumerable<Priority> List(string userId);
        IEnumerable<Priority> ActualPriorities(string userId);
        Priority AddOrUpdate(Priority priority);
        Priority ChangeName(int id, string userId, string newName);
        Priority Delete(int id, string userId);
    }

    public class PrioritiesService : IPrioritiesService
    {
        protected readonly IApplicationDbContext context;

        public PrioritiesService(IApplicationDbContext context)
        {
            this.context = context;
        }


        public Priority Find(int id)
        {
            return context.Priorities.Find(id);
        }
        
        public IEnumerable<Priority> List(string userId)
        {
            return context.Priorities.Where(p => p.UserId == userId && p.State == DbEntityState.Active);
        }

        public IEnumerable<Priority> ActualPriorities(string userId)
        {
            return
                context.Priorities.Include(p => p.Notes)
                    .Where(
                        p =>
                            p.UserId == userId &&
                            (p.State == DbEntityState.Active || p.Notes.Any(n => n.State != DbEntityState.Deleted)));
        }

        public Priority AddOrUpdate(Priority priority)
        {
            try
            {
                context.Priorities.AddOrUpdate(priority);
                context.SaveChanges();
                return priority;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Priority ChangeName(int id, string userId, string newName)
        {
            var entity = Find(id);
            if (entity == null || entity.UserId != userId || string.IsNullOrWhiteSpace(newName))
            {
                return null;
            }
            entity.Name = newName;
            return AddOrUpdate(entity);
        }

        public Priority Delete(int id, string userId)
        {
            var entity = Find(id);
            if (entity == null || entity.UserId != userId)
            {
                return null;
            }
            entity.State = DbEntityState.Deleted;
            return AddOrUpdate(entity);
        }
    }
}