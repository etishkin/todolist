﻿using System.Linq;
using System.Web.Mvc;
using ToDoList.Core.Models;
using ToDoList.Core.Services;

namespace ToDoList.Site.Controllers
{
    public class PrioritiesController : BasicController
    {
        protected readonly IPrioritiesService PrioritiesService;

        public PrioritiesController(IPrioritiesService prioritiesService)
        {
            PrioritiesService = prioritiesService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult List()
        {
            var priorities = PrioritiesService.List(CurrentUserId);
            return Json(priorities.Select(p => new { id = p.Id, name = p.Name }), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActualList()
        {
            var priorities = PrioritiesService.ActualPriorities(CurrentUserId);
            return Json(priorities.Select(p => new { id = p.Id, name = p.Name }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Add(string name)
        {
            var priority = new Priority { Name = name, UserId = CurrentUserId };
            var result = PrioritiesService.AddOrUpdate(priority);
            return Json(result.ToJson(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Update(int id, string name)
        {
            var result = PrioritiesService.ChangeName(id, CurrentUserId, name);
            return Json(result.ToJson(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var result = PrioritiesService.Delete(id, CurrentUserId);
            return Json(result.ToJson(), JsonRequestBehavior.AllowGet);
        }
    }
}