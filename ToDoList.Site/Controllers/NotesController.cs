﻿using System;
using System.Linq;
using System.Web.Mvc;
using ToDoList.Core.Models;
using ToDoList.Core.Services;
using ToDoList.Site.Utils;

namespace ToDoList.Site.Controllers
{
    [Authorize]
    public class NotesController : BasicController
    {
        protected readonly INotesService NotesService;

        public NotesController(INotesService notesService)
        {
            NotesService = notesService;
        }

        [HttpGet]
        public ActionResult List(string search = "", int? priority = null, bool state = true, string time = "desc")
        {
            if (!time.Equals("asc", StringComparison.OrdinalIgnoreCase) &&
                !time.Equals("desc", StringComparison.OrdinalIgnoreCase))
            {
                time = "desc";
            }

            var notes = new Filterable<Note>(NotesService.List(CurrentUserId));
            var result =
                notes.By(n => string.IsNullOrWhiteSpace(search) || (n.Name.Contains(search) || n.Description.Contains(search)))
                    .By(n => !priority.HasValue || priority == 0 || n.PriorityId == priority)
                    .By(n => n.State == (state ? DbEntityState.Active : DbEntityState.Inactive))
                    .OrderBy(n => n.DateCreated, time).Get();

            return Json(result.ToList().Select(n => n.ToJson()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddOrUpdate([Bind(Include = "Id, Name, PriorityId, Description")]Note note)
        {
            note.UserId = CurrentUserId;

            var result = NotesService.AddOrUpdate(note);
            return Json(result.ToJson(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var result = NotesService.Delete(id, CurrentUserId);
            return Json(result.ToJson(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Complete(int id)
        {
            var result = NotesService.Complete(id, CurrentUserId);
            return Json(result.ToJson(), JsonRequestBehavior.AllowGet);
        }
    }
}