﻿using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ToDoList.Site.Controllers
{
    public abstract class BasicController : Controller
    {
        protected string CurrentUserId
        {
            get { return HttpContext.User.Identity.GetUserId(); }
        }
    }
}