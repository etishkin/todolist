﻿$(document).ready(function () {
    refreshList();

    $('#add-priority-modal').validate({
        rules: {
            "name": {
                required: true
            }
        },
        messages: {
            "name": {
                required: "Name is required"
            }
        },
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).closest('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).closest('.form-group').removeClass(errorClass);
        }
    });
});

function refreshList() {
    $.ajax({
        url: '/priorities/list',
        success: function (data) {
            $('#list').empty();
            if (data.length) {
                $.each(data, function (i, item) {
                    addPriority(item);
                });
            } else {
                $('#list').append($('<div class="not-found"><i class="glyphicon glyphicon-search"></i>Not found :(</div>'));
            }
        }
    });
}

$('#add-button').on('click', function () {
    var $form = $(this).closest('form');
    if ($form.valid()) {
        var name = $('#name').val();
        var id = parseInt($('#id').val());
        console.log(id);
        $.ajax({
            url: id === 0 ? '/priorities/add' : '/priorities/update',
            method: 'POST',
            data: id === 0 ? { name: name } : { id: id, name: name },
            success: function (data) {
                refreshList();
                $('#add-priority-modal').modal('hide');
            }
        });
    }
});

$('#list').on('click', '.btn-edit', function () {
    var $that = $(this);
    var id = $that.closest('.priority-record').data('id');
    var name = $that.closest('.priority-record').find('span').text();
    $('#id').val(id);
    $('#name').val(name);
    $('#add-priority-modal').modal('show');

});

$('#list').on('click', '.btn-delete', function () {
    var $that = $(this);
    var id = $that.closest('.priority-record').data('id');
    $.ajax({
        url: '/priorities/delete',
        method: 'POST',
        data: { id: id },
        success: function () {
            $that.closest('.priority-record').remove();
        }
    });
});

function addPriority(model) {
    var $record = $('<div class="priority-record" data-id="' + model.id + '">');
    $record.append($('<div class="record-controls">' +
        '<a class="icon-btn btn-edit" tooltip="Edit"><i class="glyphicon glyphicon-pencil"></i></a>' +
        '<a class="icon-btn btn-delete" tooltip="Delete"><i class="glyphicon glyphicon-remove"></i></a></div>'));
    $record.append($('<span>' + model.name + '</span>'));
    $('#list').append($record);
}