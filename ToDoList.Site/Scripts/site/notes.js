﻿'use strict'
; (function () {
    $(document).ready(function () {
        refreshList();
        $.ajax({
            url: '/priorities/actuallist',
            success: function (data) {
                var $filter = $('#priority-filter');
                $filter.empty();
                $filter.append($('<option value="0">Any</option>'));
                $.each(data, function (i, item) {
                    $filter.append($('<option value="' + item.id + '">' + item.name + '</option>'));
                });
            }
        });
        $.ajax({
            url: '/priorities/list',
            success: function (data) {
                var $select = $('.priority-select');
                $select.empty();
                $.each(data, function (i, item) {
                    $select.append($('<option value="' + item.id + '">' + item.name + '</option>'));
                });
            }
        });

        $('#add-note-modal').validate({
            rules: {
                "name": {
                    required: true
                },
                "priority" : {
                    required: true
                }
            },
            messages: {
                "name" : {
                    required: "Name is required"
                },
                "priority": {
                    required: "Priority is required"
                }
            },
            errorClass: "has-error",
            highlight: function (element, errorClass) {
                $(element).closest('.form-group').addClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).closest('.form-group').removeClass(errorClass);
            }
        });
    });

    function displayRecord(model) {
        var $record = $('<div class="note-record" data-id="' + model.id + '">');
        var $div = $('<div>');
        $div.append($('<div class="record-controls">' +
            (model.state == 0
                ? '<a class="icon-btn btn-confirm" tooltip="Confirm"><i class="glyphicon glyphicon-ok"></i></a>' +
                    '<a class="icon-btn btn-edit" tooltip="Edit"><i class="glyphicon glyphicon-pencil"></i></a>'
                : '<a class="icon-btn btn-remove" tooltip="Remove"><i class="glyphicon glyphicon-remove"></i></a>') +
            '</div>'));
        $div.append($('<span class="record-priority" data-id="' + model.priorityId + '">' + model.priority + '</span>'));
        $div.append($('<span class="record-name">' + model.name + '</span>'));
        $div.append($('<span class="record-time">' + moment(model.time).fromNow() + '</span>'));
        $record.append($div);
        $record.append($('<div class="record-description" style="display:none;">' + model.description + '</div>'));
        $('#notes-list').append($record);
    }

    function refreshList() {
        $('#notes-list').empty();
        var filters = getFilters();
        console.log(filters);
        $.ajax({
            url: '/notes/list',
            data: filters,
            success: function (model) {
                if (model.length) {
                    $.each(model, function(i, item) {
                        displayRecord(item);
                    });
                } else {
                    $('#notes-list').append($('<div class="not-found"><i class="glyphicon glyphicon-search"></i>Not found :(</div>'));
                }
            }
        });
    }

    $('#notes-list').on('click', '.note-record', function (e) {
        $(this).find('.record-description').slideToggle(300);
    });

    $('#notes-list').on('click', '.btn-confirm', function (e) {
        var $record = $(this).closest('.note-record');
        var id = $record.data('id');
        $.ajax({
            url: '/notes/complete',
            method: 'POST',
            data: { id: id },
            success: function () {
                $record.remove();
            }
        });
        e.stopPropagation();
    });

    $('#notes-list').on('click', '.btn-edit', function (e) {
        var $record = $(this).closest('.note-record');
        var model = {
            id: $record.data('id'),
            name: $record.find('.record-name').text(),
            priority: $record.find('.record-priority').data('id'),
            description: $record.find('.record-description').text()
        };
        console.log(model);
        $('#priority').val(model.priority);
        $('#id').val(model.id);
        $('#name').val(model.name);
        $('#description').val(model.description);
        $('#add-note-modal').modal('show');
        e.stopPropagation();
    });

    $('#notes-list').on('click', '.btn-remove', function (e) {
        var $record = $(this).closest('.note-record');
        var id = $record.data('id');
        $.ajax({
            url: '/notes/delete',
            method: 'POST',
            data: { id: id },
            success: function () {
                $record.remove();
            }
        });
        e.stopPropagation();
    });

    $('#add-note').on('click', function () {
        $('#add-note-modal').modal('show');
    });

    function getData() {
        return {
            id: $('#id').val(),
            name: $('#name').val(),
            priorityId: $('#priority').val(),
            description: $('#description').val()
        };
    }

    function clearForm() {
        $('#id').val(0);
        $('#name').val('');
        $('#description').val('');
    }

    $('#btn-add-note').on('click', function () {
        var $form = $(this).closest('form');
        if ($form.valid()) {
            var data = getData();
            $.ajax({
                url: '/notes/AddOrUpdate',
                method: 'POST',
                data: data,
                success: function() {
                    refreshList();
                    clearForm();
                    $("#add-note-modal").modal("hide");
                }
            });
        }
    });

    $('#priority-filter').change(function () {
        refreshList();
    });

    $('#search').change(function () {
        refreshList();
    });

    $('#time-filter').on('click', function () {
        $(this).find('#direction').toggleClass('glyphicon-menu-down glyphicon-menu-up');
        refreshList();
    });

    $('#state').on('click', function () {
        $(this).toggleClass('inactive');
        $(this).text($(this).hasClass('inactive') ? "inactive" : "active");
        refreshList();
    });

    function getFilters() {
        return {
            priority: $('#priority-filter').val(),
            search: $('#search').val(),
            state: !$('#state').hasClass('inactive'),
            time: $('#direction').hasClass('glyphicon-menu-down') ? 'desc' : 'asc'
        };
    }
})(jQuery);