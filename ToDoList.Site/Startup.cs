﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ToDoList.Site.Startup))]
namespace ToDoList.Site
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
