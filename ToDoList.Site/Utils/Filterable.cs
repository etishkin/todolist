﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Ajax.Utilities;

namespace ToDoList.Site.Utils
{
    public interface IFilterable<T>
    {
        IFilterable<T> By(Antlr.Runtime.Misc.Func<T, bool> predicate);
        IFilterable<T> OrderBy<TKey>(Antlr.Runtime.Misc.Func<T, TKey> keySelector, string direction);
        IEnumerable<T> Get();
    }

    public class Filterable<T> : IFilterable<T>
    {
        private IEnumerable<T> _collection;

        public Filterable(IEnumerable<T> collection)
        {
            _collection = collection;
        }

        public IFilterable<T> By(Antlr.Runtime.Misc.Func<T, bool> predicate)
        {
            _collection = _collection.Where(arg => predicate(arg));
            return this;
        }

        public IFilterable<T> OrderBy<TKey>(Antlr.Runtime.Misc.Func<T, TKey> keySelector, string direction)
        {
            if (direction.Equals("asc", StringComparison.OrdinalIgnoreCase))
            {
                _collection = _collection.OrderBy(arg => keySelector(arg));
            }
            else if (direction.Equals("desc", StringComparison.OrdinalIgnoreCase))
            {
                _collection = _collection.OrderByDescending(arg => keySelector(arg));
            }
            else
            {
                throw new ArgumentException();
            }
            return this;
        }

        public IEnumerable<T> Get()
        {
            return _collection;
        }
    } 
}